/**
 * @description 动态表单相关接口
 * @author xzp
 */

import request from '@/utils/request'
import qs from 'qs'


/**
 * form表单添加数据
 *
 * @export
 * @return {*}
 */
export function formValueAdd(data) {
  return request({
    url: 'formValue/add',
    method: 'POST',
    data: data,
    headers: {'Content-Type': 'application/json'}
  })
}
/**
 * 查询form
 *
 * @export
 * @return {*}
 */
export function formDefControl(data) {
  return request({
    url: 'formDef/queryControl',
    method: 'POST',
    data: qs.stringify(data)
  })
}