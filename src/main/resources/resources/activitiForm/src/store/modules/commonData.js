/**
 * @description 全局数据（级联下拉，行业。。）
 * @author zr
 */

// import { getDistrict, getDepartment, getAllUserType } from '@/api/common'

const state = {
  districtList: [],
  departmentList: [],
  userType: []
}
const mutations = {
  SET_DROPLIST: (state, list) => {
    state.districtList = list
  },

  SET_DEPARTMENT: (state, list) => {
    state.departmentList = list
  },

  SET_USERTYPE: (state, list) => {
    state.userType = list
  }
}

const actions = {
  // getDistrict({ commit }) {
  //   return new Promise((resolve, reject) => {
  //     getDistrict()
  //       .then((res) => {
  //         commit('SET_DROPLIST', res.data)
  //         sessionStorage.setItem('district', JSON.stringify(res.data))
  //         resolve(res.data)
  //       })
  //       .catch((error) => {
  //         reject(error)
  //       })
  //   })
  // },
  // async getDepartment({ commit }) {
  //   return new Promise((resolve, reject) => {
  //     getDepartment()
  //       .then((res) => {
  //         commit('SET_DEPARTMENT', res.data)
  //         sessionStorage.setItem('department', JSON.stringify(res.data))
  //         resolve(res.data)
  //       })
  //       .catch((error) => {
  //         reject(error)
  //       })
  //   })
  // },
  // async getUserType({ commit }) {
  //   return new Promise((resolve, reject) => {
  //     getAllUserType()
  //       .then((res) => {
  //         commit('SET_USERTYPE', res.data)
  //         sessionStorage.setItem('userType', JSON.stringify(res.data))
  //         resolve(res.data)
  //       })
  //       .catch((error) => {
  //         reject(error)
  //       })
  //   })
  // }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
